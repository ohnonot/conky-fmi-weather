width5=50 -- width of 5th column: time and date
width4=45 -- width of 4th column: Precipitation
width3=42 -- width of 3rd column: wind_img, also right-alignment for wind text
f_s=12 -- font small
f_l=20 -- font large
o_r2=25 -- offset 2nd row
--
-- first column (image only)
--
tab=%padding_h%
draw_line(tab,%offset%,%width%-%padding_h%*2,2,'%linecol%',0.4,CAIRO_LINE_CAP_BUTT)
draw_svg_file('%smart_img%',tab,%offset%-(%img_size_fc%-%forecast_height%),%img_size_fc%,%img_size_fc%)
--
-- second column
--
tab=tab+%img_size_fc%
draw_text('%ss_text%',tab,%offset%+%forecast_height%-f_s-5,'%font%',f_s,'%textcol%',nil,nil,nil,%width%-tab-%padding_h%)
draw_text('fl %FeelsLike%',tab,%offset%+o_r2,'%font%',f_s,'%textcol%')
draw_text('%Temperature%',tab,%offset%+1,'%font%',f_l,'%tempcol%',nil,CAIRO_FONT_WEIGHT_BOLD)
--
-- third column - wind
--
tab=%width%-%padding_h%-width5-width4-width3
draw_svg_file('%wind_img%',tab+3,%offset%+3,36,36)
draw_text('%WindSpeedUnit% %WindCompass8%',tab,%offset%+o_r2,'%font%',f_s,'%textcol%',nil,nil,"right")
draw_text('%WindSpeedMS%',tab,%offset%+1,'%font%',f_l,'%textcol%',nil,CAIRO_FONT_WEIGHT_BOLD,"right")
--
-- fourth column - Precipitation
--
tab=%width%-%padding_h%-width5-width4
if string.len("%popbg%") > 2 then
   draw_box(tab,%offset%+4,width4,22,'%popbg%')
end
draw_text('%PoP%',tab,%offset%+1,'%font%',f_l,'%popcol%',nil,CAIRO_FONT_WEIGHT_BOLD)
-- drawing text + backgroundbox + bar, alpha depends on PoP
precipmm=tonumber("%Precipitation1h%")
w = draw_text('%Precipitation1h%mm',tab,%offset%+o_r2,'%font%',f_s,'%textcol%',precipmm*2+0.25)
draw_box(tab,%offset%+o_r2+f_s+1,w+1,7,'%textcol%',0.2)
draw_box(tab,%offset%+o_r2+f_s+2,precipmm*20,6,'%popcol%')
--
-- last column: time and weekday
--
tab=%width%-%padding_h%-width5
draw_text('%localtime%',%width%-%padding_h%,%offset%,'%font%',f_l,'%linecol%',0.5,CAIRO_FONT_WEIGHT_BOLD,"right")
if string.len("%day%") > 0 then
   draw_text('%day%',%width%-%padding_h%-1,%offset%+20,'%font%',f_l,'%linecol%',0.5,CAIRO_FONT_WEIGHT_BOLD,"right")
end
