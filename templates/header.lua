-- your conky.lua will need to load a file that contains the functions used here
-- add e.g. "lua_load = './files/functions.lua'," to your conky.config section.
-- Mostly two functions, one to draw an (SVG) image at a certain location, one to
-- draw text in a certain size, font and color, at a certain location.
--
--~ draw_text('%name%, %region%',%width%-%padding_h%-1,5,'%font%',16,'%textcol%',50,nil,"right")
draw_text('%name%',%padding_h%,5,'%font%',16,'%textcol%',50,CAIRO_FONT_WEIGHT_BOLD)
